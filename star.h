#ifndef _STAR_H
#define _STAR_H

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * StarTYPE																				          *
 *																									    *
 * These functions deal with the dynamically generated stars used in the game  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


typedef struct STAR_TYPE
{
	int x,y;        // position of star
   int vel;        // horizontal velocity of star
} STAR, *STAR_PTR;


STAR_TYPE  stars [256];


void Draw_Stars ( BITMAP * buffer );
void Move_Stars ( );
void Init_Stars ( );


#include "star.cpp"
#endif
