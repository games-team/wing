//=======================================================
AnimationTYPE :: AnimationTYPE ( )
	: mySize ( 0 ), myImageBank ( 0 )
{ }
//=======================================================
AnimationTYPE :: AnimationTYPE ( char * fileName )
	: mySize ( 0 ), myImageBank ( 0 )
{
   apstring source;
   ifstream ImageFile;
   ImageFile.open ( fileName );
   ImageFile >> mySize;
   myImageBank.resize ( mySize);

   if ( ImageFile.fail () )
   	abort ();

   for (int index = 0;  index < mySize; index++)
   {
      ImageFile >> source;
      myImageBank [index] = load_bmp ((char*) source.c_str(), myPal);
      cout << "Loaded OK" << endl;
   }
   ImageFile.close ( );   
}
//=======================================================
AnimationTYPE :: ~AnimationTYPE ( )
{ }
//=======================================================
void AnimationTYPE :: LoadImages ( char * fileName )
{
   apstring source;
   BITMAP * Temp = NULL;
   ifstream ImageFile;
   ImageFile.open ( fileName );
   ImageFile >> mySize;
   myImageBank.resize ( mySize);
   for ( int i = 0; i < mySize-1; i++ )
   {
     if ( myImageBank[i] != NULL )
       myImageBank[i] = NULL;
   }

   if ( ImageFile.fail () )
   	abort ();

   int index = 0;
   while ( index < mySize )
   {
   	ImageFile >> source;
      myImageBank [index] = load_bmp ((char*) source.c_str(), myPal);
      index++;
   }
}
//=======================================================
BITMAP  * AnimationTYPE :: GetImage   ( int Index ) const
{
    return myImageBank [Index];
}
//=======================================================
int AnimationTYPE :: Length ( ) const
{
	return mySize;
}
//=======================================================
RGB * AnimationTYPE :: GetPallete ( )
{
    return myPal;
}
//=======================================================
