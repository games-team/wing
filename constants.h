/*  Copyright (C) 2000 Anil Shrestha and Adam Hiatt
*   This program is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2, or (at your option)
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.*/


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Constants																				     *
 *                                                                           *
 * This file contains all the constants used throughout the entire game		  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#ifndef _CONSTANTS_H
#define _CONSTANTS_H

 	const int SCREEN_WIDTH    		= 640;				// width of screen
	const int SCREEN_HEIGHT   		= 480;         	// height of screen

   const int SIZE_OF_PALETTE 		= 256;				// number of colors in palette

   const int BUTTON_LEFT     		= 91;					// the constants that begin
   const int BUTTON_RIGHT    		= 330;				// with BUTTON_ are all
   const int BUTTON_1_TOP    		= 63;					// x and y coordinates
   const int BUTTON_1_BOTTOM 		= 88;					// for the placement of the
   const int BUTTON_2_TOP   		= 123;				// main menu
   const int BUTTON_2_BOTTOM 		= 163;
   const int BUTTON_3_TOP    		= 182;
   const int BUTTON_3_BOTTOM 		= 230;
   const int BUTTON_4_TOP    		= 250;
   const int BUTTON_4_BOTTOM 		= 278;

   const int HS_BUTTON_LEFT      = 38; 				// the constants that begin
   const int HS_BUTTON_RIGHT     = 216;            // with HS_BUTTON_ are all
   const int HS_BUTTON_1_TOP     = 233;            // x and y coordinates
   const int HS_BUTTON_1_BOTTOM  = 265;            // for the placement of the
   const int HS_BUTTON_2_TOP     = 273;            // high score menu
   const int HS_BUTTON_2_BOTTOM  = 313;

   const int O_BUTTON_LEFT      = 38;              // the constants that begin
   const int O_BUTTON_RIGHT     = 279;             // with O_BUTTON_ are all
   const int O_BUTTON_1_TOP     = 236;             // x and y coordinates
   const int O_BUTTON_1_BOTTOM  = 262;             // for the placement of the
   const int O_BUTTON_2_TOP     = 275;             // option menu
   const int O_BUTTON_2_BOTTOM  = 300;
   const int O_BUTTON_3_TOP     = 313;
   const int O_BUTTON_3_BOTTOM  = 339;
   const int O_BUTTON_4_TOP     = 350;
   const int O_BUTTON_4_BOTTOM  = 377;


   const int NUM_STARS       		= 256;            // number of stars onscreen at a time

	const int X_VELOCITY 			= 9;              // x velocity for plane
	const int Y_VELOCITY 			= 9;              // y velocity for plane

   const int ENEMY_X_VELOCITY 	= 8;              // x velocity for enemies
	const int ENEMY_Y_VELOCITY 	= 8;              // y velocity for enemies

	const int PLANE_WIDTH			= 64;             // The number of y rows in the mask
   const int MAX_NUM_BULLETS 		= 15;             // max number of bullets in a single instance of bullettype
  	const int NUM_HIGH_SCORES 		= 10; 	         // number of high scores
   const int MAX_NUM_LIVES       = 3;              // number of lives in game
   const int NUM_WEAPONS			= 4;					// total number of weapons
   const int PLANE_COLLIDE_DAMAGE = 100;				// damage incurred in a collision
   const int BULLET_PAUSE 			= 300;				// milliseconds between firing
   const int DEFAULT_HEALTH	   = 100;				// health that you start with
	const int TOTAL_NUM_STATES    = 6; 					// total number of AI states for enemies
   const int ENEMY_EXPLODE_STAGES = 26;				// number of stages in an explosion of an enemy craft
   const int PLANE_EXPLODE_STAGES = 26;				// number of stages in an explosion of the plane
   const int INIT_ENEMY_Y			 = -80;				// initial  y pos. for enemies
   const int NUM_LEVELS				 = 6;					// number of game levels
   const int NEW_LEVEL_PAUSE      = 150;				// time that new level screen is displayed
   const int BEGIN_GAME_PAUSE     = 140;				// time that begin game screen is displayed

   #define _getw(x) getw(x)								// to make stream read work
   const int STREAM_BUFFER_SIZE  = 16384; 			// for sound stream read function
   const int DEFAULT_STREAM_VOL  = 255;         	// default volume for bg. music
   const int DEFAULT_SAMPLE_VOL  = 200; 				// default volume for sound effects


   const int MAX_SPRITES  			= 10;           	// CHANGE THIS!!
	const int NUM_CHUNKS  			= 4;             	// The number of x 32 bit chunks in the mask
	const int NUM_Y  					= 64;            	// The number of y rows in the mask


   const int EnemyLifeValues [4] = {15, 25, 50, 100};			// amount of life for enemies
	const int DamageValues [4] = {5,10,20,40};					// damage for weapons
	const int KillValues [4] = {20,50,75,100};         		// points for kills
	const int YBulletVelocities [4] = {10,15,10,5};          // initial x velocities for bullets
	const int XBulletVelocities [4] = {0,0,0,0};             // initial y velocities for bullets
	const int EnemyWidths[4] = {64,60,70,58};                // widths of enemy sprites
	const int EnemyHeights[4] = {42,34,39,76};					// heights of enemy sprites
   const int NumStateStages[7] = {300,400,500,300,400,500,60}; 	// number of stages in each AI state

	enum WeaponEnum { laser, minigun, plasma, torpedo };
	enum EnemyEnum  { scout, fighter, bomber, destroyer };
	enum StateEnum  { ATTACKING_1, ATTACKING_2, ATTACKING_3,ATTACKING_4,ATTACKING_5, ATTACKING_6 };

#endif
