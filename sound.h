#ifndef _STREAM_H
#define _STREAM_H

#include <stdio.h>
#include <string.h>
#include "constants.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Stream.h																				        *
 *                                                                           *
 * These functions deal with the streaming audio used to play the background *
 * music during the game. There is a load function, a song select, an unload *
 * and a callback that is used to fetch new song data								  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typedef struct WFXSTREAM
{
  char sig[10];
  int len;
  int bits;
  int stereo;
  int freq;
  int bps;
  int played;
} WFXSTREAM;

AUDIOSTREAM *stream = NULL;
FILE        *stream_file  = NULL;
WFXSTREAM    wfx;
int 			stream_vol = DEFAULT_STREAM_VOL;
int 			sample_vol = DEFAULT_SAMPLE_VOL;

void install_stream();
void stream_start();
void stream_select_mfx( char *name );
void stream_stop();


#include "sound.cpp"
#endif
