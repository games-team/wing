#ifndef _COLLISION_H
#define _COLLISION_H

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Collision.h																				     *
 *																									  *
 * These functions handle collision detection. They provide a mixture of 	  *
 * bounding box collision detection and (if the former returns true) pixel   *
 * perfect collision detection.															  *	
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#include "constants.h"
#include "additional/apvector.h"


struct spr_mask      		// Sprite Mask
{
 int bb_height; 				// Bounding-box height of sprite
 int bb_width; 				// Bounding-box width of sprite
 int max_chunk; 				// The number of the last 32 bit chunk in the sprite's bitmap
 unsigned long int sp_mask[NUM_Y][NUM_CHUNKS]; // The (NUM_Y x (NUM_CHUNKS x 32)) bit Sprite Mask
};

void init_sp_bb( apvector<spr_mask> & mask_list  );
void mk_spr_mask           (BITMAP *s3, spr_mask & mask);
int  check_ppcollision     (const spr_mask & mask1 , int spr1x, int spr1y, const spr_mask & mask2, int spr2x, int spr2y);

#include "collision.cpp"
#endif