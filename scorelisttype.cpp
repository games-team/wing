//==========================================================================
ScoreListTYPE :: ScoreListTYPE ()
{
	list.resize (NUM_HIGH_SCORES);
	ResetList();
}
//==========================================================================
ScoreListTYPE :: ~ScoreListTYPE ()
{ }
//==========================================================================
void ScoreListTYPE :: ResetList ()
{
	for (int i = 0; i < NUM_HIGH_SCORES; i ++)
   {
		list[i].name = "anonymous";
      list[i].score = 0;
	}
}
//==========================================================================
void ScoreListTYPE :: ReadFromFile ( char * file_name )
{
	ifstream score_file;
   score_file.open (file_name);

   // I love c++
   for ( int i=0; i < NUM_HIGH_SCORES && score_file >> list[i].name >> list[i].score; i++ )
     	;
   score_file.close ();
}
//==========================================================================
void ScoreListTYPE :: DisplayList ( BITMAP * buffer )
{
 	char temp_c_str [10];
	for ( int i = 1; i <= NUM_HIGH_SCORES; i++ )
   {
      apstring converter ("");
      converter = NumToString ( i );
      converter += '.';

      strcpy (temp_c_str, converter.c_str());
   	textprintf(buffer, font , 300 , 150+20*i, 15, temp_c_str );
      strcpy (temp_c_str,list [i-1].name.c_str());
   	textprintf(buffer, font , 328 , 150+20*i, 15,  temp_c_str );
      converter = NumToString (list [i-1].score);
      strcpy (temp_c_str,converter.c_str());
   	textprintf(buffer, font , 430 , 150+20*i, 15,  temp_c_str );
   }

}
//==========================================================================
void ScoreListTYPE :: WriteToFile ( char * file_name )
{
	ofstream score_file;
   score_file.open (file_name);
   for ( int i = 0; i < NUM_HIGH_SCORES; i++ )
		score_file << list [i].name <<" "<<list[i].score << endl;
   score_file.close ();
}
//==========================================================================
void ScoreListTYPE :: InsertNewEntry ( const apstring & name, int score )
{
   apstring temp_name = name;
   int temp_score = score;
	int i;

	for ( i = 0; i < NUM_HIGH_SCORES && list[i].score > temp_score; i++ )
   	;

	for ( ; i < NUM_HIGH_SCORES ; i ++ )
   {
		Swap ( list[i].name,temp_name );
		Swap ( list[i].score,temp_score );
   }                                     
}
//==========================================================================
int ScoreListTYPE :: Lowest_Score ()
{
	return list [NUM_HIGH_SCORES-1].score;
}
//==========================================================================
