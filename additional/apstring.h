#ifndef _APSTRING_H
#define _APSTRING_H

#include <iostream.h>
#include "bool.h"

//****************************************************************
//
// apstring class
//
//****************************************************************

extern const int npos;  // Returned by the find function if
                        //   the target char or substring is not found

class apstring {

  public:

  // Constructors/destructor:

    apstring();           // Default constructor: builds an
                          //   empty string (of 0 length and capacity)
    apstring(const char * s);
                          // Builds a string from a null-terminated
                          //   string (e.g., a literal string)

    apstring(const apstring &str);
                          // Copy constructor: builds a string equal to
                          //  a given string

    ~apstring();          // Destructor

  // Assignment:

    const apstring &operator= (const apstring &str);
                          // str1 = str2;
    const apstring &operator= (const char *s);
                          // str = "Hello";
    const apstring &operator= (char ch);
                          // str = ch;

  // Overloaded subscripting operators (perform subscript range checking):

    char operator[] (int k) const;   // for use as an rvalue
    char &operator[] (int k);        // for use as lvalue

  // Append operators:

    const apstring &operator+= (const apstring &str);
                          // str1 += str2;
    const apstring &operator+= (char ch);
                          // str += ch;
        
  // Other member functions:

    int length() const;       // Returns the current length of the string
    int find(const apstring &str) const;
                              // Returns index of first occurrence of str
    int find(char ch) const;  // Returns index of first occurrence of ch
    apstring substr(int pos, int len) const;
                              // Builds and returns a substring of len chars,
                              //   starting at pos
    const char *c_str() const;
                              // Returns const pointer to the
                              //   null-terminated string in storage buffer

  private:

      int mLength;    // Current length of the string (number of characters)
      int mCapacity;  // The size of the string buffer
      char *mCstring; // Pointer to a null-terminated string
                      //   in the storage buffer
};

//****************************************************************
//*********     Free-standing operators and functions    *********
//****************************************************************

// I/O functions:

ostream &operator<< (ostream &os, const apstring &str);
istream &operator>> (istream &is, apstring &str); 
istream &getline(istream &is, apstring &str);  
          
// Concatenation operators:

apstring operator+ (const apstring &str1, const apstring &str2);
apstring operator+ (char ch, const apstring &str);
apstring operator+ (const apstring &str, char ch);

// Relational operators:

bool operator== (const apstring &str1, const apstring &str2);  
bool operator!= (const apstring &str1, const apstring &str2);  
bool operator<  (const apstring &str1, const apstring &str2);  
bool operator<= (const apstring &str1, const apstring &str2);  
bool operator>  (const apstring &str1, const apstring &str2);  
bool operator>= (const apstring &str1, const apstring &str2);

#include "apstring.cpp"
// Continued in apstring.cpp...


#endif // _APSTRING_H

